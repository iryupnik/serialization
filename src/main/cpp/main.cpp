#include <iostream>

#include <json/json.h>

#include "avro/Encoder.hh"
#include "avro/Decoder.hh"
#include "avro/ValidSchema.hh"
#include "avro/Compiler.hh"
#include "avro/DataFile.hh"

#include "dataset.h"
#include "result.h"

#include "adataset.h"
#include "measurements.pb.h"
#include "measurements.pb.cc"

#include <boost/asio.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <numeric>

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream) {
    int64_t message_size;
    stream.read((char *) &message_size, sizeof(message_size));
    message_size = be64toh(message_size);
    char *buffer = new char[message_size];
    stream.read(buffer, message_size);

    auto in = avro::memoryInputStream((uint8_t *) buffer, message_size);
    avro::DecoderPtr d = avro::binaryDecoder();
    d->init(*in);
    c::ADatasetList datasets;
    avro::decode(*d, datasets);

    c::AResultList results;
    for (c::ADataset dataset : datasets.datasets) {
        c::AResult result;
        result.info = dataset.info;
        result.results.DOWNLOAD = std::accumulate(dataset.records.DOWNLOAD.begin(), dataset.records.DOWNLOAD.end(), 0.0) / dataset.records.DOWNLOAD.size();
        result.results.PING = std::accumulate(dataset.records.PING.begin(), dataset.records.PING.end(), 0.0) / dataset.records.PING.size();
        result.results.UPLOAD = std::accumulate(dataset.records.UPLOAD.begin(), dataset.records.UPLOAD.end(), 0.0) / dataset.records.UPLOAD.size();
        results.results.emplace_back(result);
    }

    auto o = avro::memoryOutputStream(1);
    avro::EncoderPtr encoder = avro::binaryEncoder();
    encoder->init(*o);
    avro::encode(*encoder, results);
    uint32_t bufferSize = o->byteCount();
    auto data = avro::snapshot(*o);
    stream.write(reinterpret_cast<char*>(data->data()), bufferSize);
}

void processProtobuf(tcp::iostream& stream){
    int64_t message_size;
    stream.read((char *) &message_size, sizeof(message_size));
    message_size = be64toh(message_size);
    char *buffer = new char[message_size];
    stream.read(buffer, message_size);

    esw::PDatasetList datasetList;
    datasetList.ParseFromArray(buffer, message_size);

    esw::PResultList resultList;
    for (auto &dataset : datasetList.datasets()){
        esw::PResult *result = resultList.add_results();
        result->mutable_info()->CopyFrom(dataset.info());
        for (auto &record: dataset.records()) {
            auto average = result->add_averages();
            average->set_key(record.key());
            double av = std::accumulate(record.value().begin(), record.value().end(), 0.0) / record.value().size();
            average->set_value(av);
        }
    }

    resultList.SerializePartialToOstream(&stream);

}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }

    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    boost::to_upper(protocol);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            cout << "Waiting for message in " + protocol + " format..." << endl;
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "JSON"){
                processJSON(stream);
            }else if(protocol == "AVRO"){
                processAvro(stream);
            }else if(protocol == "PROTO"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
