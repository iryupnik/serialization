/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef ADATASET_H_1524381325__H_
#define ADATASET_H_1524381325__H_


#include <sstream>
#include "boost/any.hpp"
#include "avro/Specific.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

namespace c {
struct AMeasurementInfo {
    int32_t id;
    int64_t timestamp;
    std::string measurerName;
    AMeasurementInfo() :
        id(int32_t()),
        timestamp(int64_t()),
        measurerName(std::string())
        { }
};

struct AMeasurementRecords {
    std::vector<double > DOWNLOAD;
    std::vector<double > UPLOAD;
    std::vector<double > PING;
    AMeasurementRecords() :
        DOWNLOAD(std::vector<double >()),
        UPLOAD(std::vector<double >()),
        PING(std::vector<double >())
        { }
};

struct ADataset {
    AMeasurementInfo info;
    AMeasurementRecords records;
    ADataset() :
        info(AMeasurementInfo()),
        records(AMeasurementRecords())
        { }
};

struct ADatasetList {
    std::vector<ADataset > datasets;
    ADatasetList() :
        datasets(std::vector<ADataset >())
        { }
};

struct AMeasurementResults {
    double DOWNLOAD;
    double UPLOAD;
    double PING;
    AMeasurementResults() :
        DOWNLOAD(double()),
        UPLOAD(double()),
        PING(double())
        { }
};

struct AResult {
    AMeasurementInfo info;
    AMeasurementResults results;
    AResult() :
        info(AMeasurementInfo()),
        results(AMeasurementResults())
        { }
};

struct AResultList {
    std::vector<AResult > results;
    AResultList() :
        results(std::vector<AResult >())
        { }
};

struct measurements_avsc_Union__0__ {
private:
    size_t idx_;
    boost::any value_;
public:
    size_t idx() const { return idx_; }
    AMeasurementInfo get_AMeasurementInfo() const;
    void set_AMeasurementInfo(const AMeasurementInfo& v);
    ADataset get_ADataset() const;
    void set_ADataset(const ADataset& v);
    ADatasetList get_ADatasetList() const;
    void set_ADatasetList(const ADatasetList& v);
    AResult get_AResult() const;
    void set_AResult(const AResult& v);
    AResultList get_AResultList() const;
    void set_AResultList(const AResultList& v);
    measurements_avsc_Union__0__();
};

inline
AMeasurementInfo measurements_avsc_Union__0__::get_AMeasurementInfo() const {
    if (idx_ != 0) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AMeasurementInfo >(value_);
}

inline
void measurements_avsc_Union__0__::set_AMeasurementInfo(const AMeasurementInfo& v) {
    idx_ = 0;
    value_ = v;
}

inline
ADataset measurements_avsc_Union__0__::get_ADataset() const {
    if (idx_ != 1) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADataset >(value_);
}

inline
void measurements_avsc_Union__0__::set_ADataset(const ADataset& v) {
    idx_ = 1;
    value_ = v;
}

inline
ADatasetList measurements_avsc_Union__0__::get_ADatasetList() const {
    if (idx_ != 2) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADatasetList >(value_);
}

inline
void measurements_avsc_Union__0__::set_ADatasetList(const ADatasetList& v) {
    idx_ = 2;
    value_ = v;
}

inline
AResult measurements_avsc_Union__0__::get_AResult() const {
    if (idx_ != 3) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResult >(value_);
}

inline
void measurements_avsc_Union__0__::set_AResult(const AResult& v) {
    idx_ = 3;
    value_ = v;
}

inline
AResultList measurements_avsc_Union__0__::get_AResultList() const {
    if (idx_ != 4) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResultList >(value_);
}

inline
void measurements_avsc_Union__0__::set_AResultList(const AResultList& v) {
    idx_ = 4;
    value_ = v;
}

inline measurements_avsc_Union__0__::measurements_avsc_Union__0__() : idx_(0), value_(AMeasurementInfo()) { }
}
namespace avro {
template<> struct codec_traits<c::AMeasurementInfo> {
    static void encode(Encoder& e, const c::AMeasurementInfo& v) {
        avro::encode(e, v.id);
        avro::encode(e, v.timestamp);
        avro::encode(e, v.measurerName);
    }
    static void decode(Decoder& d, c::AMeasurementInfo& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.id);
                    break;
                case 1:
                    avro::decode(d, v.timestamp);
                    break;
                case 2:
                    avro::decode(d, v.measurerName);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.id);
            avro::decode(d, v.timestamp);
            avro::decode(d, v.measurerName);
        }
    }
};

template<> struct codec_traits<c::AMeasurementRecords> {
    static void encode(Encoder& e, const c::AMeasurementRecords& v) {
        avro::encode(e, v.DOWNLOAD);
        avro::encode(e, v.UPLOAD);
        avro::encode(e, v.PING);
    }
    static void decode(Decoder& d, c::AMeasurementRecords& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.DOWNLOAD);
                    break;
                case 1:
                    avro::decode(d, v.UPLOAD);
                    break;
                case 2:
                    avro::decode(d, v.PING);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.DOWNLOAD);
            avro::decode(d, v.UPLOAD);
            avro::decode(d, v.PING);
        }
    }
};

template<> struct codec_traits<c::ADataset> {
    static void encode(Encoder& e, const c::ADataset& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.records);
    }
    static void decode(Decoder& d, c::ADataset& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.records);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.records);
        }
    }
};

template<> struct codec_traits<c::ADatasetList> {
    static void encode(Encoder& e, const c::ADatasetList& v) {
        avro::encode(e, v.datasets);
    }
    static void decode(Decoder& d, c::ADatasetList& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.datasets);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.datasets);
        }
    }
};

template<> struct codec_traits<c::AMeasurementResults> {
    static void encode(Encoder& e, const c::AMeasurementResults& v) {
        avro::encode(e, v.DOWNLOAD);
        avro::encode(e, v.UPLOAD);
        avro::encode(e, v.PING);
    }
    static void decode(Decoder& d, c::AMeasurementResults& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.DOWNLOAD);
                    break;
                case 1:
                    avro::decode(d, v.UPLOAD);
                    break;
                case 2:
                    avro::decode(d, v.PING);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.DOWNLOAD);
            avro::decode(d, v.UPLOAD);
            avro::decode(d, v.PING);
        }
    }
};

template<> struct codec_traits<c::AResult> {
    static void encode(Encoder& e, const c::AResult& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.results);
    }
    static void decode(Decoder& d, c::AResult& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.results);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.results);
        }
    }
};

template<> struct codec_traits<c::AResultList> {
    static void encode(Encoder& e, const c::AResultList& v) {
        avro::encode(e, v.results);
    }
    static void decode(Decoder& d, c::AResultList& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.results);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.results);
        }
    }
};

template<> struct codec_traits<c::measurements_avsc_Union__0__> {
    static void encode(Encoder& e, c::measurements_avsc_Union__0__ v) {
        e.encodeUnionIndex(v.idx());
        switch (v.idx()) {
        case 0:
            avro::encode(e, v.get_AMeasurementInfo());
            break;
        case 1:
            avro::encode(e, v.get_ADataset());
            break;
        case 2:
            avro::encode(e, v.get_ADatasetList());
            break;
        case 3:
            avro::encode(e, v.get_AResult());
            break;
        case 4:
            avro::encode(e, v.get_AResultList());
            break;
        }
    }
    static void decode(Decoder& d, c::measurements_avsc_Union__0__& v) {
        size_t n = d.decodeUnionIndex();
        if (n >= 5) { throw avro::Exception("Union index too big"); }
        switch (n) {
        case 0:
            {
                c::AMeasurementInfo vv;
                avro::decode(d, vv);
                v.set_AMeasurementInfo(vv);
            }
            break;
        case 1:
            {
                c::ADataset vv;
                avro::decode(d, vv);
                v.set_ADataset(vv);
            }
            break;
        case 2:
            {
                c::ADatasetList vv;
                avro::decode(d, vv);
                v.set_ADatasetList(vv);
            }
            break;
        case 3:
            {
                c::AResult vv;
                avro::decode(d, vv);
                v.set_AResult(vv);
            }
            break;
        case 4:
            {
                c::AResultList vv;
                avro::decode(d, vv);
                v.set_AResultList(vv);
            }
            break;
        }
    }
};

}
#endif
