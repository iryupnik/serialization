package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.json.MeasurementInfo;
import cz.esw.serialization.proto.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {

	private InputStream is;
	private OutputStream os;
	private Map<Integer, Dataset> datasetMap;

	public ProtoDataHandler(InputStream is, OutputStream os) {
		this.is = is;
		this.os = os;
	}

	@Override
	public void initialize() {
		datasetMap = new HashMap<>();
	}

	@Override
	public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
		MeasurementInfo measurementInfo = new MeasurementInfo(datasetId, timestamp, measurerName);
		Dataset dataset = new Dataset(measurementInfo, new EnumMap<>(DataType.class));
		datasetMap.put(datasetId, dataset);
	}

	@Override
	public void handleValue(int datasetId, DataType type, double value) {
		if (datasetMap.get(datasetId) == null) {
			throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
		}
		datasetMap.get(datasetId).getRecords().computeIfAbsent(type, t -> new ArrayList<>()).add(value);
	}

	@Override
	public void getResults(ResultConsumer consumer) {
		PDatasetList.Builder datasetList = PDatasetList.newBuilder();
		for (Dataset dataset : datasetMap.values()) {
			PDataset.Builder pDataset = PDataset.newBuilder()
					.setInfo(PMeasurementInfo.newBuilder()
							.setId(dataset.getInfo().id())
							.setTimestamp(dataset.getInfo().timestamp())
							.setMeasurerName(dataset.getInfo().measurerName())
							.build());
			for (Map.Entry<DataType, List<Double>> entry : dataset.getRecords().entrySet()) {
				pDataset.addRecords(PMapFieldEntry.newBuilder()
                        .setKey(dataTypeToPDataType(entry.getKey()))
                        .addAllValue(entry.getValue())
                        .build());
			}
			datasetList.addDatasets(pDataset.build());
		}
		PDatasetList datasetListProto = datasetList.build();

		try {
			os.write(ByteBuffer.allocate(8).putLong(datasetListProto.getSerializedSize()).array());
			datasetListProto.writeTo(os);
			os.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		try {
			PResultList results = PResultList.parseFrom(is);
			for (PResult result : results.getResultsList()) {
				PMeasurementInfo info = result.getInfo();
				consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
				for (PMapEntry entry : result.getAveragesList()) {
                    consumer.acceptResult(pDataTypeToDataType(entry.getKey()), entry.getValue());
                }

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private PDataType dataTypeToPDataType(DataType dataType){
		return switch (dataType) {
			case DOWNLOAD -> PDataType.DOWNLOAD;
			case UPLOAD -> PDataType.UPLOAD;
			case PING -> PDataType.PING;
		};
	}

	private DataType pDataTypeToDataType(PDataType pDataType){
        return switch (pDataType) {
            case DOWNLOAD -> DataType.DOWNLOAD;
            case UPLOAD -> DataType.UPLOAD;
            case PING -> DataType.PING;
			default -> null;
        };
    }
}
