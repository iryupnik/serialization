package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.ADataset;
import cz.esw.serialization.avro.ADatasetList;
import cz.esw.serialization.avro.AMeasurementInfo;
import cz.esw.serialization.avro.AMeasurementRecords;
import cz.esw.serialization.avro.AResultList;
import cz.esw.serialization.avro.AResult;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.MeasurementInfo;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificDatumReader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

    private InputStream is;
    private OutputStream os;
    Map<Integer, ADataset> datasets;

    public AvroDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void initialize() {
        datasets = new HashMap<>();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        AMeasurementInfo measurementInfo = new AMeasurementInfo();
        measurementInfo.setId(datasetId);
        measurementInfo.setMeasurerName(measurerName);
        measurementInfo.setTimestamp(timestamp);

        AMeasurementRecords measurementRecords = new AMeasurementRecords();
        measurementRecords.setDOWNLOAD(new ArrayList<>());
        measurementRecords.setPING(new ArrayList<>());
        measurementRecords.setUPLOAD(new ArrayList<>());

        ADataset dataset = new ADataset();
        dataset.setInfo(measurementInfo);
        dataset.setRecords(measurementRecords);

        datasets.put(datasetId, dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        ADataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }

        switch (type) {
            case PING -> dataset.getRecords().getPING().add(value);
            case UPLOAD -> dataset.getRecords().getUPLOAD().add(value);
            case DOWNLOAD -> dataset.getRecords().getDOWNLOAD().add(value);
        }
    }

    @Override
    public void getResults(ResultConsumer consumer) {
        ADatasetList datasetList = new ADatasetList();
        datasetList.setDatasets(new ArrayList<>(datasets.values()));
        DatumWriter<ADatasetList> writer = new SpecificDatumWriter<>(ADatasetList.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(baos, null);
        try {
            writer.write(datasetList, encoder);
            encoder.flush();
            int size = baos.size();
            byte[] bytes = baos.toByteArray();
            os.write(ByteBuffer.allocate(8).putLong(size).array());
            os.write(bytes);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AResultList resultList = new AResultList();
        resultList.setResults(new ArrayList<>());
        try {
            Decoder decoder = DecoderFactory.get().binaryDecoder(this.is, null);
            DatumReader<AResultList> reader = new SpecificDatumReader<>(AResultList.class);
            resultList = reader.read(null, decoder);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (AResult result : resultList.getResults()) {
            AMeasurementInfo info = result.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());
            consumer.acceptResult(DataType.PING, result.getResults().getPING());
            consumer.acceptResult(DataType.DOWNLOAD, result.getResults().getDOWNLOAD());
            consumer.acceptResult(DataType.UPLOAD, result.getResults().getUPLOAD());
        }
    }
}
